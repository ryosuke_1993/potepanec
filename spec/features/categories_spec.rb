require 'rails_helper'

RSpec.describe "Categories", type: :feature do
  let(:taxonomy) { create(:taxonomy, name: "categories") }
  let(:taxonomy2) { create(:taxonomy, name: "brands") }
  let(:taxon) { create(:taxon, name: 'bags', parent: taxonomy.root, taxonomy: taxonomy) }
  let(:taxon2) { create(:taxon, name: 'mugs', parent: taxonomy.root, taxonomy: taxonomy) }
  let!(:product) { create(:product, name: "test-BAG", price: "19.99", taxons: [taxon]) }
  let!(:product2) { create(:product, name: "test-MUG", price: "25.00", taxons: [taxon2]) }

  before do
    visit potepan_category_path(taxon.id)
  end

  scenario "View show page" do
    expect(page).to have_title "#{taxon.name} - BIGBAG Store"
    expect(page).to have_selector ".page-title h2", text: taxon.name
    expect(page).not_to have_selector ".page-title h2", text: taxon2.name
    expect(page).to have_content "shop"
    expect(page).to have_selector ".productCaption h5", text: product.name
    expect(page).not_to have_selector ".productCaption h5", text: product2.name
    expect(page).to have_selector ".productCaption h3", text: product.price
    expect(page).not_to have_selector ".productCaption h3", text: product2.price
  end

  scenario "click side bar" do
    find("#category-#{taxonomy.id}").click
    expect(page).to have_selector "#taxon#{taxon.id}", text: "bags (1)"
    expect(page).to have_selector "#taxon#{taxon2.id}", text: "mugs (1)"
  end

  it "has correct categories menu" do
    expect(page).to have_content, text: taxon.name
    expect(page).to have_content, text: taxon2.name
  end

  it "click product name" do
    expect(page).to have_link product.name, href: potepan_product_path(product.id)
  end

  it "click category name" do
    expect(page).to have_link taxon.name, href: potepan_category_path(taxon.id)
  end
end
