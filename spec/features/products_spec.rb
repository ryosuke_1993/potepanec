require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let(:taxonomy) { create(:taxonomy, name: "category") }
  let(:taxon) { create(:taxon, name: 'bags', parent: taxonomy.root, taxonomy: taxonomy) }
  let(:taxon2) { create(:taxon, name: 'mags', parent: taxonomy.root, taxonomy: taxonomy) }
  let(:product) { create(:product, name: "BAG", price: "20.00", taxons: [taxon]) }
  let!(:related_product) { create(:product, taxons: [taxon]) }
  let!(:unrelated_product) { create(:product, taxons: [taxon2]) }
  let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }

  before do
    visit potepan_product_path(product.id)
  end

  scenario "View show page" do
    expect(page).to have_current_path potepan_product_path(product.id)
    expect(page).to have_title "#{product.name} - BIGBAG Store"
    expect(page).to have_selector ".page-title h2", text: product.name
    expect(page).to have_selector ".col-xs-6 h2", text: product.name
    expect(page).to have_selector ".col-xs-6 h2", text: product.name
    expect(page).to have_selector ".media-body h2", text: product.name
    expect(page).to have_selector ".media-body h3", text: product.display_price
    expect(page).to have_selector ".media-body p", text: product.description
    expect(page).to have_link 'Home', href: potepan_index_path
    expect(page).to have_link '一覧ページへ戻る', href: potepan_category_path(taxon.id)
    expect(page).to have_selector 'h5', text: related_product.name
    expect(page).not_to have_selector 'h5', text: product.name
    expect(page).not_to have_selector 'h5', text: unrelated_product.name
  end

  it "click related_product name" do
    expect(page).to have_link related_product.name, href: potepan_product_path(related_product.id)
  end

  it "has the correct related_products" do
    expect(all('.productBox').size).to eq 4
  end
end
