require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "#full_title(page_title)" do
    context "product is BAG" do
      example "return BAG - BIGBAG Store" do
        expect(full_title("BAG")).to eq "BAG - BIGBAG Store"
      end
    end

    context "other" do
      example "return BIGBAG Store" do
        expect(full_title("")).to eq "BIGBAG Store"
        expect(full_title(nil)).to eq "BIGBAG Store"
      end
    end
  end
end
