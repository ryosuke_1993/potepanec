require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe "related_products" do
    let(:taxon) { create(:taxon) }
    let(:taxon2) { create(:taxon) }
    let(:selected_product) { create(:product, taxons: [taxon]) }
    let(:related_product) { create(:product, taxons: [taxon]) }
    let(:unrelated_product) { create(:product, taxons: [taxon2]) }

    context "related_product choose corect products" do
      it "has related_product and not have same product" do
        expect(selected_product.related_products).to match [related_product]
      end

      it "not have unrelated_product" do
        expect(selected_product.related_products).not_to match_array [unrelated_product]
      end

      it "not have selected_product self" do
        expect(selected_product.related_products).not_to match_array [selected_product]
      end

      it "return empty if product does not have related_products" do
        expect(unrelated_product.related_products).to be_empty
      end
    end
  end
end
