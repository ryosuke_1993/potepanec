require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "#show" do
    let(:taxonomy) { create(:taxonomy, name: "category") }
    let(:taxon) { create(:taxon, name: 'bags', parent: taxonomy.root, taxonomy: taxonomy) }
    let(:product) { create(:product, name: "BAG", taxons: [taxon]) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "returns http success" do
      expect(response).to have_http_status "200"
    end

    it "return correct products name" do
      expect(response.body).to include "BAG"
    end

    it "return correct taxonomy name" do
      expect(response.body).to include "categories"
    end

    it "return correct products categolies" do
      expect(response.body).to include 'bags'
    end
  end
end
