require 'rails_helper'

RSpec.describe "Potepan::ProductsController", type: :request do
  describe "#show" do
    let(:taxonomy) { create(:taxonomy, name: "category") }
    let(:taxon) { create(:taxon, name: 'bags', parent: taxonomy.root, taxonomy: taxonomy) }
    let(:product) { create(:product, name: "BAG", price: "20.00",  taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it "responds successfully" do
      expect(response).to have_http_status "200"
    end

    it "has the product price" do
      expect(response.body).to include product.price.to_s
    end

    it "has the correct full_title and product name" do
      expect(response.body).to include product.name
    end

    it "has the correct description" do
      expect(response.body).to include product.description
    end
  end
end
